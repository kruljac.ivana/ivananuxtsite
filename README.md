# ivanaNuxtApp

This is a Portfolio website

I did this site in order to play with `nuxt.js` and to see what this framework is capable of  

![Screenshot](static/img/ivana-theme.jpg)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

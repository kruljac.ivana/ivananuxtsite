// import axios from 'axios'
require('dotenv').config()

export default {
    mode: 'universal',
    head: {
        title: process.env.MAIN_TITLE || '',
        meta: [{
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0'
            }
        ],
        htmlAttrs: {
            lang: 'en'
        },
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }],
        script: [
            {
                src: 'https://code.jquery.com/jquery-3.4.1.min.js',
                type: 'text/javascript',
                integrity: 'sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=',
                crossorigin: 'anonymus',
                body: true
            },
            //-- static directory
            {
                src: '/js/grained.min.js',
                type: 'text/javascript',
                body: true,
                defer: true
            },
            {
                src: '/js/typed.min.js',
                type: 'text/javascript',
                body: true,
                defer: true
            },
            {
                src: '/js/progressbar.min.js',
                type: 'text/javascript',
                body: true,
                defer: true
            },
            {
                src: '/js/main.min.js',
                type: 'text/javascript',
                body: true,
                defer: true
            }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#fff'
    },

    /*
     ** Global CSS
     */
    css: [
        '@assets/scss/app.scss'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        { src: '~/plugins/jsonld' },
        { src: '~plugins/ga.js', mode: 'client' }
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv',
        '@nuxtjs/svg'
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {},
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extractCSS: true,
        extend(config, ctx) {}
    }
}

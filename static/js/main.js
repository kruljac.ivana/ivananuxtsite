$(document).ready(function () {

    $('.lines').addClass('finish')
    setTimeout(function() {
        $('.lines').addClass('ready')
    }, 2000)

    var width = $(window).width();

    if (width > 1199) {
        var grainedOptions = {
            "animate": true,
            "patternWidth": 400,
            "patternHeight": 400,
            "grainOpacity": 0.25,
            "grainDensity": 3.5,
            "grainWidth": 1,
            "grainHeight": 1
        }
        grained("#intrograined", grainedOptions);
    }

    var typedOptiones = {
        strings:[
            "Hello <span>webspace</span> wanderer <span>!</span>",
            "I'm a <span>Full Stack</span> Web Developer <span>.</span>",
            "I <span>♥</span> coding <span>.</span>",
            "Frontend <span>is my</span> playground <span>.</span>",
            "I <span>♥</span> Node.js <span>.</span>",
            "I <span>♥</span> Wordpress <span>.</span>",
        ],
        backSpeed: 30,
        typeSpeed: 50,
        loop: true,
        startDelay: 2700,
        showCursor: false,
        //cursorChar: '|',
        autoInsertCss: true,
        fadeOut: true,
        fadeOutClass: 'typed-fade-out',
    }
    var typed = new Typed('.type-intro', typedOptiones);

        // -- cursor folower
    if ((width > 1199) && $('.cursor-follower').length) {
        $(window).on('mousemove', function(e) {
            var x = e.pageX;
            var y = e.pageY;
            var newposX = x;
            var newposY = y;
            $('.cursor-follower').css('transform', 'translate3d(' + newposX + 'px,' + newposY + 'px,0px)');
        })
        $('a').on({
            mouseenter: function(e) {
                cursor_over();
            },
            mouseleave: function(e) {
                cursor_out();
            }
        })
    }
    function cursor_over() {
        $(".cursor-follower-inner").stop().animate({
            width: 80,
            height: 80,
            opacity: 0.75,
            margin: '-40px 0 0 -40px'
        }, 500)
    }
    function cursor_out() {
        $(".cursor-follower-inner").stop().animate({
            width: 26,
            height: 26,
            opacity: 0.55,
            margin: '-13px 0 0 -13px'
        }, 500)
    }

    // Display the progress bar calling progressbar.js
    $('.progressbar').progressBar({
        shadow : true,
        percentage : false,
        animation : true
    })

    //-- header navigation scroller
    $('header li.inav a[href^="#"]').click(function(e) {
        $('html, body').animate({ scrollTop: $(this.hash).offset().top + 60 }, 1000)
        return false
        e.preventDefault()
    })

    //-- scroll to top
    $('header a[href^="#home"]').click(function(f) {
        $('html, body').animate({ scrollTop: 0 }, 1000)
        return false
        f.preventDefault()
    })

    //-- add class to header on scroll
    var scrolling = false
    $(window).on('scroll', function() {
        scrolling = true
    })
    setInterval(function() {
        if (scrolling) {
            scrolling = false
            var y = $(document).scrollTop()
            if (y > $('header').height()) {
                $('header').addClass('slideIn')
            } else {
                $('header').removeClass('slideIn')
            }

        }
    }, 250)
})
